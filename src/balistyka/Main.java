package balistyka;

//import javax.swing.*;

public class Main {

	public static void main(String[] args) {
		Bullet[] pew = new Bullet[3];
		pew[0] = new BB();
		pew[1] = new Cal45ACP();
		pew[2] = new Cal22LR();
		
		for(Bullet i:pew){
			((ModelG1)i).model();                //Wywo�anie metody interfejsu ModelG1
			((ModelG7)i).step1m();               //                            ModelG7 - niezaimplementowana
		}
	}
}

/*
Dodano Interfejsy ModelG7, reprezentuj�cy u�ywany dotychczas model, oraz ModelG1, tymczasowo niezaimplementowany w �adnej z klas i domy�lnie wyrzucaj�cy komunikat.
Funkcja symulacji 1m lotu ju� nie jest funkcj� wirtualn� dziedziczon� z Bullet, jest teraz cz�ci� interfejsu modelu.
Pozwala to na symulowanie lotu pocisk�w r�nych kalibr�w w jednolity spos�b, a jednocze�nie z zachowaniem ich unikalnych w�asno�ci.
*/