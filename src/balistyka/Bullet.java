package balistyka;

public abstract class Bullet {
	//public double m;          //masa w g
	public Vector pos;        //pozycja w przestrzeni
	public Vector vel;        //predkosc
	public double E;          //energia kinetyczna w joulach
	public double t;          //czas od wystrzalu
	
	public Bullet() {
		pos = new Vector();
		vel = new Vector();
	};
	public void move(double dx, double dy, double dz){                        //przesuniecie o wektor [dx dy]
		pos.x = pos.x+dx;
		pos.y = pos.y+dy;
		pos.z = pos.z+dz;
	}
	public void place(double new_x, double new_y, double new_z){                 //umieszczenie w puncie
		pos.x = new_x;
		pos.y = new_y;
		pos.z = new_z;
	}	
	public abstract void fire(double angle);             //strzal z predkoscia charakterystyczna
                                                         //dla pocisku z zadanym katem
	public void fire() {fire(0);}
	//public abstract void step1m();      
	public void print(){
		System.out.format("t = %.4f x = %4.4f y = %4.4f z = %4.4f  vx = %4.4f vy = %4.4f vz = %4.4f E = %4.4f%n", t, pos.x, pos.y, pos.z, vel.x, vel.y, vel.z, E);	
	}
	
	public void print_m(){
		System.out.format("%f, %f, %f;%n",pos.x,pos.y,pos.z);
	}
	
}
