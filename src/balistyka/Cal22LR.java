package balistyka;

public class Cal22LR extends Bullet implements ModelG7, ModelG1{
	public static final double initialVelocity = 384;
	public static final double dragCoefficient = 0.9885;
	public static final double mass = 2.3;
	public static final String name = ".22LR";
	@Override
	public void step1m(){            
		Vector v0;                                                       
		double dt;
		
		v0 = vel;
		
		E = dragCoefficient*E;                         //E koncowa
		vel.scale(Math.sqrt(2000*E/mass)/v0.length());          //predkosc koncowa
		dt = 2/(v0.x+vel.x);
		t = t + dt;              //czas
		
		vel.z = vel.z - 9.80665*dt;
		
		pos.x = pos.x + (vel.x+v0.x)/2*dt;
		pos.y = pos.y + (vel.y+v0.y)/2*dt;
		pos.z = pos.z + (vel.z+v0.z)/2*dt;
	}
	
	public void fire(double angle){
		double deg = angle/360*2*Math.PI;
		vel.x = initialVelocity*Math.cos(deg);
		vel.y = 0;
		vel.z = initialVelocity*Math.sin(deg);
		
		E = mass*vel.length()*vel.length()/2000;
	}
}
