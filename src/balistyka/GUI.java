package balistyka;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Component;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.SpringLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JCheckBox;

import org.jfree.chart.ChartPanel; 
import org.jfree.chart.JFreeChart; 
import org.jfree.data.xy.XYSeries; 
import org.jfree.chart.plot.XYPlot; 
import org.jfree.chart.ChartFactory; 
import org.jfree.data.xy.XYSeriesCollection; 
import java.util.Scanner;
import java.awt.Dimension;

public class GUI {

	private JFrame frame;
	private JTextField textMass;
	private JTextField textVelocity;
	private JTextField textDrag;
	private JTextField textNazwa;
	private int nextSeriesID;
	private String bulletType;
	double mass; 
	double vel;
	double drag;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		nextSeriesID = 0;
		bulletType = "inny";
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setMinimumSize(new Dimension(500, 350));
		frame.setBounds(100, 100, 704, 506);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		
		
		
		XYSeriesCollection xydataset = new XYSeriesCollection();
		
		XYPlot xyplot = new XYPlot();
		xyplot.setDataset(xydataset);
		
		JFreeChart jFreeChart = ChartFactory.createXYLineChart(null, null, null, xydataset);
		
		ChartPanel chartPanel = new ChartPanel(jFreeChart);
		springLayout.putConstraint(SpringLayout.NORTH, chartPanel, 10, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, chartPanel, -10, SpringLayout.EAST, frame.getContentPane());
		chartPanel.setVisible(true);
		
		frame.getContentPane().add(chartPanel);
		
		JPanel panelParameters = new JPanel();
		springLayout.putConstraint(SpringLayout.SOUTH, panelParameters, 150, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, panelParameters, 150, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, chartPanel, 10, SpringLayout.EAST, panelParameters);
		springLayout.putConstraint(SpringLayout.NORTH, panelParameters, 10, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, panelParameters, 10, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(panelParameters);
		SpringLayout sl_panelParameters = new SpringLayout();
		panelParameters.setLayout(sl_panelParameters);
		
		JLabel labelMass = new JLabel("masa:");
		sl_panelParameters.putConstraint(SpringLayout.NORTH, labelMass, 5, SpringLayout.NORTH, panelParameters);
		sl_panelParameters.putConstraint(SpringLayout.WEST, labelMass, 5, SpringLayout.WEST, panelParameters);
		labelMass.setHorizontalAlignment(SwingConstants.LEFT);
		panelParameters.add(labelMass);
		
		textMass = new JTextField();
		sl_panelParameters.putConstraint(SpringLayout.NORTH, textMass, 2, SpringLayout.SOUTH, labelMass);
		sl_panelParameters.putConstraint(SpringLayout.WEST, textMass, 5, SpringLayout.WEST, panelParameters);
		panelParameters.add(textMass);
		textMass.setHorizontalAlignment(SwingConstants.LEFT);
		textMass.setColumns(10);
		
		JLabel labelVelocity = new JLabel("pr\u0119dko\u015B\u0107 pocz\u0105tkowa:");
		sl_panelParameters.putConstraint(SpringLayout.NORTH, labelVelocity, 5, SpringLayout.SOUTH, textMass);
		sl_panelParameters.putConstraint(SpringLayout.WEST, labelVelocity, 5, SpringLayout.WEST, panelParameters);
		panelParameters.add(labelVelocity);
		
		textVelocity = new JTextField();
		sl_panelParameters.putConstraint(SpringLayout.NORTH, textVelocity, 2, SpringLayout.SOUTH, labelVelocity);
		sl_panelParameters.putConstraint(SpringLayout.WEST, textVelocity, 5, SpringLayout.WEST, panelParameters);
		panelParameters.add(textVelocity);
		textVelocity.setColumns(10);
		
		JLabel labelDrag = new JLabel("wsp\u00F3\u0142czynnik oporu:");
		sl_panelParameters.putConstraint(SpringLayout.NORTH, labelDrag, 5, SpringLayout.SOUTH, textVelocity);
		sl_panelParameters.putConstraint(SpringLayout.WEST, labelDrag, 5, SpringLayout.WEST, panelParameters);
		sl_panelParameters.putConstraint(SpringLayout.NORTH, labelVelocity, 5, SpringLayout.SOUTH, textMass);
		sl_panelParameters.putConstraint(SpringLayout.WEST, labelVelocity, 5, SpringLayout.WEST, panelParameters);
		panelParameters.add(labelDrag);
		
		textDrag = new JTextField();
		sl_panelParameters.putConstraint(SpringLayout.NORTH, textDrag, 2, SpringLayout.SOUTH, labelDrag);
		sl_panelParameters.putConstraint(SpringLayout.WEST, textDrag, 5, SpringLayout.WEST, panelParameters);
		panelParameters.add(textDrag);
		textDrag.setColumns(10);
		
		JPanel panelBullets = new JPanel();
		springLayout.putConstraint(SpringLayout.NORTH, panelBullets, -120, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, panelBullets, 10, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, panelBullets, -10, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, panelBullets, 100, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(panelBullets);
		SpringLayout sl_panelBullets = new SpringLayout();
		panelBullets.setLayout(sl_panelBullets);
		
		JButton button22LR = new JButton(".22 LR");
		sl_panelBullets.putConstraint(SpringLayout.WEST, button22LR, 0, SpringLayout.WEST, panelBullets);
		button22LR.setAlignmentX(Component.CENTER_ALIGNMENT);
		panelBullets.add(button22LR);
		
		JButton button45ACP = new JButton(".45 ACP");
		sl_panelBullets.putConstraint(SpringLayout.SOUTH, button22LR, -2, SpringLayout.NORTH, button45ACP);
		sl_panelBullets.putConstraint(SpringLayout.WEST, button45ACP, 0, SpringLayout.WEST, panelBullets);
		panelBullets.add(button45ACP);
		button45ACP.setAlignmentX(0.5f);
		
		JButton buttonBB = new JButton("BB");
		sl_panelBullets.putConstraint(SpringLayout.SOUTH, button45ACP, -2, SpringLayout.NORTH, buttonBB);
		sl_panelBullets.putConstraint(SpringLayout.WEST, buttonBB, 0, SpringLayout.WEST, panelBullets);
		panelBullets.add(buttonBB);
		buttonBB.setAlignmentX(0.5f);
		
		JButton buttonInny = new JButton("inny");
		
		sl_panelBullets.putConstraint(SpringLayout.SOUTH, buttonBB, -2, SpringLayout.NORTH, buttonInny);
		sl_panelBullets.putConstraint(SpringLayout.SOUTH, buttonInny, 0, SpringLayout.SOUTH, panelBullets);
		sl_panelBullets.putConstraint(SpringLayout.EAST, buttonBB, 0, SpringLayout.EAST, buttonInny);
		sl_panelBullets.putConstraint(SpringLayout.EAST, button45ACP, 0, SpringLayout.EAST, buttonInny);
		sl_panelBullets.putConstraint(SpringLayout.EAST, button22LR, 0, SpringLayout.EAST, buttonInny);
		sl_panelBullets.putConstraint(SpringLayout.WEST, buttonInny, 0, SpringLayout.WEST, panelBullets);
		sl_panelBullets.putConstraint(SpringLayout.EAST, buttonInny, 90, SpringLayout.WEST, panelBullets);
		buttonInny.setAlignmentX(Component.CENTER_ALIGNMENT);
		panelBullets.add(buttonInny);
		
		textNazwa = new JTextField();
		springLayout.putConstraint(SpringLayout.WEST, textNazwa, 105, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, textNazwa, -12, SpringLayout.SOUTH, frame.getContentPane());
		sl_panelBullets.putConstraint(SpringLayout.NORTH, textNazwa, 171, SpringLayout.SOUTH, panelParameters);
		sl_panelBullets.putConstraint(SpringLayout.WEST, textNazwa, 0, SpringLayout.WEST, frame.getContentPane());
		sl_panelBullets.putConstraint(SpringLayout.SOUTH, textNazwa, -6, SpringLayout.NORTH, button22LR);
		frame.getContentPane().add(textNazwa);
		textNazwa.setToolTipText("");
		textNazwa.setText("z pliku");
		textNazwa.setColumns(10);
		
		JButton buttonSymulacja = new JButton("Symulacja");
		springLayout.putConstraint(SpringLayout.SOUTH, chartPanel, -10, SpringLayout.NORTH, buttonSymulacja);
		springLayout.putConstraint(SpringLayout.SOUTH, buttonSymulacja, -10, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, buttonSymulacja, -10, SpringLayout.EAST, frame.getContentPane());
		frame.getContentPane().add(buttonSymulacja);
		
		JCheckBox checkBox = new JCheckBox("Na\u0142\u00F3\u017C na istniej\u0105cy");
		checkBox.setHorizontalTextPosition(SwingConstants.LEFT);
		springLayout.putConstraint(SpringLayout.NORTH, checkBox, 0, SpringLayout.NORTH, buttonSymulacja);
		springLayout.putConstraint(SpringLayout.EAST, checkBox, -2, SpringLayout.WEST, buttonSymulacja);
		frame.getContentPane().add(checkBox);
		
		button22LR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textMass.setText(String.valueOf(Cal22LR.mass));
				textVelocity.setText(String.valueOf(Cal22LR.initialVelocity));
				textDrag.setText(String.valueOf(Cal22LR.dragCoefficient));
				bulletType = Cal22LR.name;
				textMass.setEditable(false);
				textVelocity.setEditable(false);
				textDrag.setEditable(false);
			}
		});
		button45ACP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textMass.setText(String.valueOf(Cal45ACP.mass));				
				textVelocity.setText(String.valueOf(Cal45ACP.initialVelocity));
				textDrag.setText(String.valueOf(Cal45ACP.dragCoefficient));
				textMass.setEditable(false);
				textVelocity.setEditable(false);
				textDrag.setEditable(false);
				bulletType = Cal45ACP.name;
			}
		});
		buttonBB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textMass.setText(String.valueOf(BB.mass));
				textVelocity.setText(String.valueOf(BB.initialVelocity));
				textDrag.setText(String.valueOf(BB.dragCoefficient));
				bulletType = BB.name;
				textMass.setEditable(false);
				textVelocity.setEditable(false);
				textDrag.setEditable(false);
			}
		});
		
		buttonInny.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				bulletType = "inny";
				textMass.setEditable(true);
				textVelocity.setEditable(true);
				textDrag.setEditable(true);
				File file = null;
				try{
					file = new File(textNazwa.getText());
				}
				catch(Exception e){return;}
				Scanner scanner = null;
				try{
					scanner = new Scanner(file);
				}
				catch(Exception e){return;}
				
				if(scanner.hasNextLine())
					bulletType = scanner.nextLine();
				try{
					mass = Double.parseDouble(scanner.next());
					drag = Double.parseDouble(scanner.next());
					vel = Double.parseDouble(scanner.next());
				}
				catch(Exception e){
					scanner.close();
					return;
				}
				
				textMass.setText(String.valueOf(mass));
				textVelocity.setText(String.valueOf(vel));
				textDrag.setText(String.valueOf(drag));
				scanner.close();				
			}
		});
		buttonSymulacja.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				try{
					mass = Double.parseDouble(textMass.getText()); 
					vel = Double.parseDouble(textVelocity.getText());
					drag = Double.parseDouble(textDrag.getText());
				}
				catch(Exception e){return;}
				
				if(!checkBox.isSelected()){
					nextSeriesID = 0;
					xydataset.removeAllSeries();
				}
				
				Bullet bullet=null;
				if(bulletType == Cal22LR.name)
					bullet = new Cal22LR();
				else if(bulletType == Cal45ACP.name)
					bullet = new Cal45ACP();
				else if(bulletType == BB.name)
					bullet = new BB();
				else
					bullet = new CustomBullet(bulletType, mass, drag, vel);
				
				XYSeries bulletData = new XYSeries(String.valueOf(nextSeriesID) +") "+ bulletType);
				nextSeriesID++;
				bullet.place(0, 0, 1);
				bullet.fire(0);
				bullet.print();	
				while(bullet.pos.z >= 0){
					((ModelG7)bullet).step1m();
					bulletData.add(bullet.pos.x, bullet.pos.z);
					if(bullet.pos.x%25 == 0)
						bullet.print();	
				}
				
				xydataset.addSeries(bulletData);
			}
		});			
	}
}
